import React, { PureComponent } from 'react';
import moment from 'moment';
import { Table, Alert, Badge, Divider, Avatar } from 'antd';
import styles from './index.less';

const statusMap = ['default', 'processing', 'success', 'error'];
class StandardTable extends PureComponent {
    state = {
      selectedRowKeys: [],
      totalCallNo: 0,
    };

    componentWillReceiveProps(nextProps) {
      // clean state
      if (nextProps.selectedRows.length === 0) {
        this.setState({
          selectedRowKeys: [],
          totalCallNo: 0,
        });
      }
    }

    handleRowSelectChange = (selectedRowKeys, selectedRows) => {
      const totalCallNo = selectedRows.reduce((sum, val) => {
        return sum + parseFloat(val.callNo, 10);
      }, 0);

      if (this.props.onSelectRow) {
        this.props.onSelectRow(selectedRows);
      }

      this.setState({ selectedRowKeys, totalCallNo });
    }

    handleTableChange = (pagination, filters, sorter) => {
      this.props.onChange(pagination, filters, sorter);
    }

    cleanSelectedKeys = () => {
      this.handleRowSelectChange([], []);
    }

    render() {
      const { selectedRowKeys, totalCallNo } = this.state;
      const { data: { list, pagination }, loading } = this.props;

      // const status = ['关闭', '运行中', '已上线', '异常'];
      const status = ['下架', '正常'];

      const columns = [
        {
          title: 'ID',
          dataIndex: 'key',
        },
        {
          title: '用户ID',
          dataIndex: 'userInfo.mid',
        },
        {
          title: '内容',
          dataIndex: 'articlesInfo.contentHead',
        },
        {
          title: '位置',
          dataIndex: 'articlesInfo.cityName',
        },
        {
          title: '携带红包',
          align: 'center',
          dataIndex: 'articlesInfo.redType',
          render(val) {
            if (val === 0) {
              return '否';
            }
            return '是';
          },
        },
        {
          title: '设备',
          dataIndex: 'articlesInfo.relOs',
        },
        {
          title: '评论数',
          align: 'center',
          dataIndex: 'articlesInfo.artData.collectSum',
        },
        {
          title: '收藏数',
          align: 'center',
          dataIndex: 'articlesInfo.artData.commentSum',
        },
        {
          title: '打赏数',
          align: 'center',
          dataIndex: 'articlesInfo.artData.exceSum',
        },
        {
          title: '点赞数',
          align: 'center',
          dataIndex: 'articlesInfo.artData.likeSum',
        },
        {
          title: '阅读数',
          align: 'center',
          dataIndex: 'articlesInfo.artData.readSum',
        },
        {
          title: '分享数',
          align: 'center',
          dataIndex: 'articlesInfo.artData.shareSum',
        },
        {
          title: '发布时间',
          dataIndex: 'articlesInfo.dateCreate',
          sorter: true,
          render: val => <span>{moment(val).format('YYYY-MM-DD HH:mm:ss')}</span>,
        },
        {
          title: '状态',
          dataIndex: 'articlesInfo.statusType',
          render(val) {
            return <Badge status={statusMap[val]} text={status[val]} />;
          },
        },
      ];

      const paginationProps = {
        showSizeChanger: true,
        showQuickJumper: true,
        ...pagination,
      };

      const rowSelection = {
        selectedRowKeys,
        onChange: this.handleRowSelectChange,
        getCheckboxProps: record => ({
          disabled: record.disabled,
        }),
      };

      return (
        <div className={styles.standardTable}>
          <div className={styles.tableAlert}>
            <Alert
              message={(
                <div>
                    已选择 <a style={{ fontWeight: 600 }}>{selectedRowKeys.length}</a> 项&nbsp;&nbsp;
                服务调用总计 <span style={{ fontWeight: 600 }}>{totalCallNo}</span> 万
                  <a onClick={this.cleanSelectedKeys} style={{ marginLeft: 24 }}>清空</a>
                </div>
                        )}
              type="info"
              showIcon
            />
          </div>
          <Table
            loading={loading}
            rowKey={record => record.key}
            rowSelection={rowSelection}
            dataSource={list}
            columns={columns}
            pagination={paginationProps}
            onChange={this.handleTableChange}
          />
        </div>
      );
    }
}

export default StandardTable;
