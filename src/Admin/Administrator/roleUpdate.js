import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Form, Input, Checkbox, DatePicker, Select, Button, Card, InputNumber, Radio, Icon, Tooltip, Spin, Tree,
} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './style.less';

const roleLoading = false;
const FormItem = Form.Item;
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const TreeNode = Tree.TreeNode;


@connect(state => ({
  detail: state.role.detail,
  detailLoading: state.role.detailLoading,
  submitting: state.role.regularFormSubmitting,
}))
@Form.create()
export default class BasicForms extends PureComponent {
    state = {
      expandedKeys: ['0-0-0', '0-0-1'],
      autoExpandParent: true,
      checkedKeys: this.props.detail.roleHasMenus,
      selectedKeys: [],
    };


    componentDidMount() {
      if (this.props.match.params.id) {
        const { dispatch } = this.props;
        dispatch({
          type: 'role/roleDetail',
          payload: {
            roleId: this.props.match.params.id,
          },
        });
      }
    }

    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (!err) {
          values.permissions = this.state.checkedKeys;
          values.roleId = this.props.detail.id;
          this.props.dispatch({
            type: 'role/edit',
            payload: values,
          });
        }
      });
    }

    onExpand = (expandedKeys) => {
      console.log('onExpand', arguments);
      // if not set autoExpandParent to false, if children expanded, parent can not collapse.
      // or, you can remove all expanded children keys.
      this.setState({
        expandedKeys,
        autoExpandParent: false,
      });
    }
    onCheck = (checkedKeys) => {
      console.log('onCheck', checkedKeys);
      this.setState({ checkedKeys });
    }
    onSelect = (selectedKeys, info) => {
      console.log('onSelect', info);
      this.setState({ selectedKeys });
    }
    renderTreeNodes = (data) => {
      // this.setState({ checkedKeys: roleHasMenus });
      return data.map((item) => {
        if (item.children) {
          return (
            <TreeNode title={item.title} key={item.key} dataRef={item}>
              {this.renderTreeNodes(item.children)}
            </TreeNode>
          );
        }
        return <TreeNode {...item} />;
      });
    }

    render() {
      const { roleMenusLoading, roleMenus, regularFormSubmitting: submitting, detailLoading, detail, detail: { roleHasMenus } } = this.props;
      const { getFieldDecorator, getFieldValue } = this.props.form;
      const CheckboxGroup = Checkbox.Group;
      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 7 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 12 },
          md: { span: 10 },
        },
      };

      const submitFormLayout = {
        wrapperCol: {
          xs: { span: 24, offset: 0 },
          sm: { span: 10, offset: 7 },
        },
      };

      return (

        <PageHeaderLayout title="创建角色">
          <Spin spinning={detailLoading}>
            <Card bordered={false}>
              <Form
                onSubmit={this.handleSubmit}
                hideRequiredMark
                style={{ marginTop: 8 }}
              >
                <FormItem
                  {...formItemLayout}
                  label="角色名称"
                >
                  {getFieldDecorator('name', {
                                    rules: [{
                                        required: true, message: '角色名称',
                                    }],
                                    initialValue: detail.name,
                                })(
                                  <Input placeholder="请输入角色名称" name="name" />
                                    )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="角色名称"
                >
                  {getFieldDecorator('note', {
                                    rules: [{
                                        required: true, message: '角色说明',
                                    }],
                                    initialValue: detail.note,
                                })(
                                  <TextArea autosize={{ minRows: 2, maxRows: 6 }} placeholder="请输入角色说明" name="note" />
                                    )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="状态"
                >
                  <div>
                    {getFieldDecorator('status', {
                                        initialValue: detail.status ? detail.status.toString() : '1',
                                    })(
                                      <Radio.Group>
                                        <Radio value="1">正常</Radio>
                                        <Radio value="2">封禁</Radio>
                                      </Radio.Group>
                                        )}
                  </div>
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="权限节点"
                >
                  {detailLoading ? '暂无' : (
                    <Tree
                      checkable
                      onExpand={this.onExpand}
                      expandedKeys={this.state.expandedKeys}
                      autoExpandParent={this.state.autoExpandParent}
                      onCheck={this.onCheck}
                      checkedKeys={this.state.checkedKeys}
                      onSelect={this.onSelect}
                      selectedKeys={this.state.selectedKeys}
                      defaultExpandAll={false}
                    >
                      {this.renderTreeNodes(detail.menus)}
                    </Tree>
                                )}
                </FormItem>
                <FormItem {...submitFormLayout} style={{ marginTop: 32 }}>
                  <Button type="primary" htmlType="submit" loading={submitting}>
                                    提交
                  </Button>
                </FormItem>
              </Form>
            </Card>
          </Spin>
        </PageHeaderLayout>
      );
    }
}
