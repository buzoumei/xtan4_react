import React, { PureComponent } from 'react';
import moment from 'moment';
import { connect } from 'dva';
import { Table, Badge, Divider, Icon, Modal, Button, message } from 'antd';
import styles from './index.less';


const statusMap = ['success', 'processing', 'default', 'error'];
@connect(state => ({
  permission: state.permission,
}))
class StandardTable extends PureComponent {
    state = {

      ModalText: null,
      visible: false,
      confirmLoading: false,
      delId: null,
    };


    showModal = (id, name) => {
      this.setState({
        visible: true,
        ModalText: `是否删除 ${name}?`,
        delId: id,
      });
    }

    handleCancel = () => {
      this.setState({
        visible: false,
        delId: null,
      });
    }

    handleOk = (delId) => {
      const { dispatch } = this.props;
      this.setState({
        ModalText: '正在删除...',
        confirmLoading: true,
      });
      dispatch({
        type: 'permission/remove',
        payload: {
          permissionId: delId,
        },
        callback: (response) => {
          if (response.status) {
            message.success(response.msg);
            dispatch({
              type: 'permission/fetch',
              payload: {
                currentPage: this.props.data.pagination.current,
                pageSize: this.props.data.pagination.pageSize,
              },
            });
          } else {
            message.error(response.msg);
          }
          this.setState({
            visible: false,
            confirmLoading: false,
          });
        },
      });
    }


    handleTableChange = (pagination, filters, sorter) => {
      this.props.onChange(pagination, filters, sorter);
    }

    cleanSelectedKeys = () => {
      this.handleRowSelectChange([], []);
    }

    render() {
      const { data: { list, pagination }, loading } = this.props;
      const { visible, confirmLoading, ModalText, delId } = this.state;
      const status = ['', '正常', '封禁'];

      const columns = [
        {
          title: 'ID',
          dataIndex: 'key',
        },
        {
          title: '名称',
          width: 150,
          align: 'center',
          dataIndex: 'name',
        },
        {
          title: '图标',
          dataIndex: 'icon',
          render: val => <Icon type={val} />,
        },
        {
          title: 'ant路径',
          dataIndex: 'path',
        },
        {
          title: '路径',
          dataIndex: 'deep',
        },
        {
          title: '路由',
          align: 'center',
          dataIndex: 'route',
        },
        {
          title: '方法',
          align: 'center',
          dataIndex: 'method',
        },
        {
          title: '创建时间',
          dataIndex: 'created_at',
          sorter: true,
          render: val => <span>{moment(val).format('YYYY-MM-DD HH:mm:ss')}</span>,
        },
        {
          title: '状态',
          dataIndex: 'status',
          filters: [
            {
              text: status[1],
              value: 1,
            },
            {
              text: status[2],
              value: 2,
            },
          ],
          render(val) {
            return <Badge status={statusMap[val]} text={status[val]} />;
          },
        },
        {
          title: '操作',
          align: 'center',
          render: (text, record) => (
            <div>
              <a href={`#/admin/permissionUpdate/${record.key}`}><Button type="primary">编辑</Button></a>
              <Divider type="vertical" />
              <Button type="danger" onClick={() => this.showModal(record.key, record.name)}>删除</Button>
            </div>
          ),
        },
      ];

      const paginationProps = {
        showSizeChanger: true,
        showQuickJumper: true,
        ...pagination,
      };


      return (
        <div className={styles.standardTable}>
          <Table
            loading={loading}
            rowKey={record => record.key}
            dataSource={list}
            columns={columns}
            pagination={paginationProps}
            onChange={this.handleTableChange}
          />
          <Modal
            title="删除"
            visible={visible}
            onOk={() => this.handleOk(delId)}
            confirmLoading={confirmLoading}
            onCancel={this.handleCancel}
          >
            <p>{ModalText}</p>
          </Modal>
        </div>
      );
    }
}

export default StandardTable;
