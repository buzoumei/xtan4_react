import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Form, Input, Checkbox, DatePicker, Select, Button, Card, InputNumber, Radio, Icon, Tooltip, TreeSelect, Spin,
} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './style.less';

const FormItem = Form.Item;
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;

@connect(state => ({
  // submitting: state.permission.regularFormSubmitting,
  permission: state.permission,
}))
@Form.create()
export default class BasicForms extends PureComponent {
    state = {
      count: 0,
      confirmDirty: false,
      visible: false,
      help: '',
      prefix: '86',
      parentId: 0,
    };

    onSelect = (val) => {
      this.setState({ parentId: val });
    }

    componentDidMount() {
      this.props.dispatch({
        type: 'permission/menus',
      });
    }

    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (!err) {
          values.parentId = this.state.parentId;
          this.props.dispatch({
            type: 'permission/add',
            payload: values,
          });
        }
      });
    }

    render() {
      const { getFieldDecorator, getFieldValue } = this.props.form;
      const { permission: { menus, menusLoading, regularFormSubmitting: submitting } } = this.props;

      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 7 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 12 },
          md: { span: 10 },
        },
      };

      const submitFormLayout = {
        wrapperCol: {
          xs: { span: 24, offset: 0 },
          sm: { span: 10, offset: 7 },
        },
      };

      return (
        <PageHeaderLayout title="创建权限菜单">
          <Spin spinning={menusLoading}>
            <Card bordered={false}>
              <Form
                onSubmit={this.handleSubmit}
                hideRequiredMark
                style={{ marginTop: 8 }}
              >
                <FormItem
                  {...formItemLayout}
                  label="name"
                >
                  {getFieldDecorator('name', {
                                    rules: [{
                                        required: true, message: 'name',
                                    }],
                                })(
                                  <Input placeholder="请输入name" name="name" />
                                    )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="route"
                >
                  {getFieldDecorator('route')(
                    <Input placeholder="请输入route" name="route" />
                                )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="method"
                >
                  {getFieldDecorator('method')(
                    <Select placeholder="请选择" style={{ width: '100%' }}>
                      <Option value="GET">GET</Option>
                      <Option value="POST">POST</Option>
                      <Option value="DELETE">DELETE</Option>
                      <Option value="PUT">PUT</Option>
                      <Option value="PATCH">PATCH</Option>
                    </Select>
                                )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="icon"
                >
                  {getFieldDecorator('icon')(
                    <Input placeholder="请输入icon" name="icon" />
                                )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="path"
                >
                  {getFieldDecorator('path', {
                                    rules: [{
                                        required: true, message: 'path',
                                    }],
                                })(
                                  <Input placeholder="请输入path" name="path" />
                                    )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="parent"
                >
                  {getFieldDecorator('parentId', {
                                    rules: [{
                                        required: true, message: '请输入parentId',
                                    }],
                                })(
                                  <TreeSelect
                                    style={{ width: 300 }}
                                    dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                    treeData={menus}
                                    placeholder="Please select"
                                    treeDefaultExpandAll
                                    onSelect={this.onSelect}
                                  />
                                    )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="isMenu"
                >
                  <div>
                    {getFieldDecorator('isMenu', {
                                        initialValue: '2',
                                    })(
                                      <Radio.Group>
                                        <Radio value="1">是</Radio>
                                        <Radio value="2">否</Radio>
                                      </Radio.Group>
                                        )}
                  </div>
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="status"
                >
                  <div>
                    {getFieldDecorator('status', {
                                        initialValue: '1',
                                    })(
                                      <Radio.Group>
                                        <Radio value="1">正常</Radio>
                                        <Radio value="2">封禁</Radio>
                                      </Radio.Group>
                                        )}
                  </div>
                </FormItem>
                <FormItem {...submitFormLayout} style={{ marginTop: 32 }}>
                  <Button type="primary" htmlType="submit" loading={submitting}>
                                    提交
                  </Button>
                  {/* <Button style={{ marginLeft: 8 }}>保存</Button> */}
                </FormItem>
              </Form>
            </Card>
          </Spin>
        </PageHeaderLayout>
      );
    }
}
