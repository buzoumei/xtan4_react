import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Form, Input, Checkbox, DatePicker, Select, Button, Card, InputNumber, Radio, Icon, Tooltip, Spin,
} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './style.less';

const FormItem = Form.Item;
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;

@connect(state => ({
  administrator: state.administrator,
  submitting: state.administrator.regularFormSubmitting,
}))
@Form.create()
export default class BasicForms extends PureComponent {
    state = {
      count: 0,
      confirmDirty: false,
      visible: false,
      help: '',
      prefix: '86',
    };

    componentDidMount() {
      if (this.props.match.params.id) {
        const { dispatch } = this.props;
        dispatch({
          type: 'administrator/administratorDetail',
          payload: {
            administratorId: this.props.match.params.id,
          },
        });
      }
    }

    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (!err) {
          values.administratorId = this.props.administrator.detail.id;
          this.props.dispatch({
            type: 'administrator/edit',
            payload: values,
          });
        }
      });
    }

    checkConfirm = (rule, value, callback) => {
      const { form } = this.props;
      if (value && value !== form.getFieldValue('password')) {
        callback('两次输入的密码不匹配!');
      } else {
        callback();
      }
    };

    checkPassword = (rule, value, callback) => {
      if (!value) {
        this.setState({
          help: '请输入密码！',
          visible: !!value,
        });
        callback('密码不能为空');
      } else {
        this.setState({
          help: '',
        });
        if (!this.state.visible) {
          this.setState({
            visible: !!value,
          });
        }
        if (value.length < 6) {
          callback('密码小于6位');
        } else {
          const { form } = this.props;
          if (value && this.state.confirmDirty) {
            form.validateFields(['password_confirmation '], { force: true });
          }
          callback();
        }
      }
    };

    render() {
      const { administrator: { detailLoading, detail, regularFormSubmitting: submitting } } = this.props;
      const { getFieldDecorator, getFieldValue } = this.props.form;
      const CheckboxGroup = Checkbox.Group;
      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 7 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 12 },
          md: { span: 10 },
        },
      };

      const submitFormLayout = {
        wrapperCol: {
          xs: { span: 24, offset: 0 },
          sm: { span: 10, offset: 7 },
        },
      };

      return (
        <PageHeaderLayout title="编辑管理员">
          <Spin spinning={detailLoading}>
            <Card bordered={false}>
              <Form
                onSubmit={this.handleSubmit}
                hideRequiredMark
                style={{ marginTop: 8 }}
              >
                <FormItem
                  {...formItemLayout}
                  label="用户名"
                >
                  {getFieldDecorator('userName', {
                                    rules: [{
                                        required: true, message: '请输入用户名',
                                    }],
                                    initialValue: detail.name,
                                })(
                                  <Input placeholder="请输入用户名" name="userName" />
                                    )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="选择角色"
                >
                  {detailLoading ? '无' : (
                                    getFieldDecorator('role', {
                                        rules: [
                                            { required: true, message: '请选择角色' },
                                        ],
                                        initialValue: detail.hasRole,
                                    })(
                                      <CheckboxGroup options={detail.roles} />
                                        )
                                )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="状态"
                >
                  <div>
                    {getFieldDecorator('status', {
                                        initialValue: detail.status ? detail.status.toString() : '1',
                                    })(
                                      <Radio.Group>
                                        <Radio value="1">正常</Radio>
                                        <Radio value="2">封禁</Radio>
                                      </Radio.Group>
                                        )}
                  </div>
                </FormItem>
                <FormItem {...submitFormLayout} style={{ marginTop: 32 }}>
                  <Button type="primary" htmlType="submit" loading={submitting}>
                                    提交
                  </Button>
                </FormItem>
              </Form>
            </Card>
          </Spin>
        </PageHeaderLayout>
      );
    }
}
