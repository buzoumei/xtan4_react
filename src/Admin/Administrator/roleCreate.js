import React, { PureComponent } from 'react';
import { connect } from 'dva';
import {
  Form, Input, Checkbox, DatePicker, Select, Button, Card, InputNumber, Radio, Icon, Tooltip, Spin, Tree,
} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './style.less';

const roleLoading = false;
const FormItem = Form.Item;
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const TreeNode = Tree.TreeNode;
const treeData = [{
  title: '0-0',
  key: '0-0',
  children: [{
    title: '0-0-0',
    key: '0-0-0',
    children: [
      { title: '0-0-0-0', key: '0-0-0-0', value: '1' },
      { title: '0-0-0-1', key: '0-0-0-1' },
      { title: '0-0-0-2', key: '0-0-0-2' },
    ],
  }, {
    title: '0-0-1',
    key: '0-0-1',
    children: [
      { title: '0-0-1-0', key: '0-0-1-0' },
      { title: '0-0-1-1', key: '0-0-1-1' },
      { title: '0-0-1-2', key: '0-0-1-2' },
    ],
  }, {
    title: '0-0-2',
    key: '0-0-2',
  }],
}, {
  title: '0-1',
  key: '0-1',
  children: [
    { title: '0-1-0-0', key: '0-1-0-0' },
    { title: '0-1-0-1', key: '0-1-0-1' },
    { title: '0-1-0-2', key: '0-1-0-2' },
  ],
}, {
  title: '0-2',
  key: '0-2',
}];

@connect(state => ({
  roleMenus: state.role.roleMenus,
  roleMenusLoading: state.role.roleMenusLoading,
  submitting: state.role.regularFormSubmitting,
}))
@Form.create()
export default class BasicForms extends PureComponent {
    state = {
      count: 0,
      confirmDirty: false,
      visible: false,
      help: '',
      prefix: '86',
      expandedKeys: ['0-0-0', '0-0-1'],
      autoExpandParent: true,
      checkedKeys: ['0-0-0'],
      selectedKeys: [],
    };


    componentDidMount() {
      this.props.dispatch({
        type: 'role/roleMenus',
      });
    }

    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (!err) {
          values.permissions = this.state.checkedKeys;
          this.props.dispatch({
            type: 'role/add',
            payload: values,
          });
        }
      });
    }

    onExpand = (expandedKeys) => {
      console.log('onExpand', arguments);
      // if not set autoExpandParent to false, if children expanded, parent can not collapse.
      // or, you can remove all expanded children keys.
      this.setState({
        expandedKeys,
        autoExpandParent: false,
      });
    }
    onCheck = (checkedKeys) => {
      console.log('onCheck', checkedKeys);
      this.setState({ checkedKeys });
    }
    onSelect = (selectedKeys, info) => {
      console.log('onSelect', info);
      this.setState({ selectedKeys });
    }
    renderTreeNodes = (data) => {
      return data.map((item) => {
        if (item.children) {
          return (
            <TreeNode title={item.title} key={item.key} dataRef={item}>
              {this.renderTreeNodes(item.children)}
            </TreeNode>
          );
        }
        return <TreeNode {...item} />;
      });
    }

    render() {
      const { roleMenusLoading, roleMenus, regularFormSubmitting: submitting } = this.props;
      // console.log(role);
      const { getFieldDecorator, getFieldValue } = this.props.form;
      const CheckboxGroup = Checkbox.Group;
      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 7 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 12 },
          md: { span: 10 },
        },
      };

      const submitFormLayout = {
        wrapperCol: {
          xs: { span: 24, offset: 0 },
          sm: { span: 10, offset: 7 },
        },
      };

      return (
        <PageHeaderLayout title="创建角色">
          <Card bordered={false}>
            <Form
              onSubmit={this.handleSubmit}
              hideRequiredMark
              style={{ marginTop: 8 }}
            >
              <FormItem
                {...formItemLayout}
                label="角色名称"
              >
                {getFieldDecorator('name', {
                                rules: [{
                                    required: true, message: '角色名称',
                                }],
                            })(
                              <Input placeholder="请输入角色名称" name="name" />
                                )}
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="角色名称"
              >
                {getFieldDecorator('note', {
                                rules: [{
                                    required: true, message: '角色说明',
                                }],
                            })(
                              <TextArea autosize={{ minRows: 2, maxRows: 6 }} placeholder="请输入角色说明" name="note" />
                                )}
              </FormItem>
              {/* <FormItem
							{...formItemLayout}
							label="选择角色"
						>
							<Spin spinning={rolesLoading}>
								{rolesLoading ? '无' : (
									getFieldDecorator('role', {
										rules: [
											{ required: true, message: '请选择角色' },
										],
									})(
										<CheckboxGroup options={roles} />
										)
								)}
							</Spin>
						</FormItem> */}
              <FormItem
                {...formItemLayout}
                label="状态"
              >
                <div>
                  {getFieldDecorator('status', {
                                    initialValue: '1',
                                })(
                                  <Radio.Group>
                                    <Radio value="1">正常</Radio>
                                    <Radio value="2">封禁</Radio>
                                  </Radio.Group>
                                    )}
                </div>
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="权限节点"
              >
                <Spin spinning={roleMenusLoading}>
                  {roleMenusLoading ? '暂无' : (
                                    // getFieldDecorator('roles', {
                                    // 	rules: [
                                    // 		{ required: true, message: '请设置权限' },
                                    // 	],
                                    // })(
                    <Tree
                      checkable
                      onExpand={this.onExpand}
                      expandedKeys={this.state.expandedKeys}
                      autoExpandParent={this.state.autoExpandParent}
                      onCheck={this.onCheck}
                      checkedKeys={this.state.checkedKeys}
                      onSelect={this.onSelect}
                      selectedKeys={this.state.selectedKeys}
                      defaultExpandAll
                    >
                      {this.renderTreeNodes(roleMenus)}
                    </Tree>
                                    // )
                                )}
                </Spin>

              </FormItem>
              <FormItem {...submitFormLayout} style={{ marginTop: 32 }}>
                <Button type="primary" htmlType="submit" loading={submitting}>
                                提交
                </Button>
                {/* <Button style={{ marginLeft: 8 }}>保存</Button> */}
              </FormItem>
            </Form>
          </Card>
        </PageHeaderLayout>
      );
    }
}
