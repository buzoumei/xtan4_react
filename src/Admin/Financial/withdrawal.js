import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Card, Button, Modal, Menu, DatePicker, message, Select } from 'antd';

import StandardTable from './withdrawal/StandardTable';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

import styles from './withdrawal.less';

const { RangePicker } = DatePicker;
const { Option } = Select;

@connect(state => ({
  withdrawal: state.withdrawal,
}))
// @Form.create()
export default class TableList extends PureComponent {
    state = {
      selectedRows: [],
      visible: false,
      confirmLoading: false,
      exportParam: {
        startDate: null,
        endDate: null,
      },
      withdrawalStatus: null,
    };


    componentDidMount() {
      const { dispatch } = this.props;
      dispatch({
        type: 'withdrawal/fetch',
      });
    }

    onChange = (date, dateString) => {
      this.setState({
        exportParam: {
          startDate: dateString[0],
          endDate: dateString[1],
        },
      });
      console.log(this.state.exportParam);
    }

    handleChange = (value) => {
      this.setState({
        withdrawalStatus: value,
      });

      console.log(`selected ${value}`);
    }

    showModal = () => {
      this.setState({
        visible: true,
      });
    }

    handleCancel = () => {
      this.setState({
        visible: false,
      });
    }

    handleOk = () => {
      const { dispatch } = this.props;
      this.setState({
        confirmLoading: true,
      });
      dispatch({
        type: 'withdrawal/export',
        payload: {
          startDate: this.state.exportParam.startDate,
          endDate: this.state.exportParam.endDate,
          status: this.state.withdrawalStatus,
        },
        callback: (response) => {
          if (response.status) {
            message.success(response.msg);
            window.open(response.list.excelUrl, '_blank');
          } else {
            message.error(response.msg);
          }
          this.setState({
            visible: false,
            confirmLoading: false,
          });
        },
      });
    }


    render() {
      const { withdrawal: { loading: ruleLoading, data } } = this.props;
      const { selectedRows } = this.state;
      const { visible, confirmLoading, delId } = this.state;


      const menu = (
        <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
          <Menu.Item key="remove">删除</Menu.Item>
          <Menu.Item key="approval">批量审批</Menu.Item>
        </Menu>
      );

      return (
        <PageHeaderLayout title="">
          <Card bordered={false}>
            <div className={styles.tableList}>
              <div className={styles.tableListForm} />
              <div className={styles.tableListOperator}>
                <Button onClick={() => this.showModal()}>excel导出</Button>
              </div>
              <StandardTable
                selectedRows={selectedRows}
                loading={ruleLoading}
                data={data}
                onSelectRow={this.handleSelectRows}
                onChange={this.handleStandardTableChange}
              />
              <Modal
                title="execl导出"
                visible={visible}
                onOk={() => this.handleOk(delId)}
                confirmLoading={confirmLoading}
                onCancel={this.handleCancel}
              >
                <p>
                选择日期区间：<RangePicker onChange={this.onChange} />
                </p>
                选择提现状态：
                <Select
                  showSearch
                  style={{ width: 335 }}
                  optionFilterProp="children"
                  name="diabledTime"
                  onChange={this.handleChange}
                >
                  <Option value="4">所有</Option>
                  <Option value="1">提现中</Option>
                  <Option value="2">提现成功</Option>
                  <Option value="3">提现失败</Option>
                </Select>

              </Modal>
            </div>
          </Card>
        </PageHeaderLayout>
      );
    }
}
