import React, { PureComponent } from 'react';
import moment from 'moment';
import { Table, Alert, Badge, Divider, Avatar } from 'antd';
import styles from './index.less';
import { stat } from 'fs';
import { platform } from 'os';

const statusMap = ['','processing', 'success', 'error', 'default'];
class StandardTable extends PureComponent {
	state = {
		selectedRowKeys: [],
		totalCallNo: 0,
	};

	componentWillReceiveProps(nextProps) {
		// clean state
		if (nextProps.selectedRows.length === 0) {
			this.setState({
				selectedRowKeys: [],
				totalCallNo: 0,
			});
		}
	}

	handleRowSelectChange = (selectedRowKeys, selectedRows) => {
		const totalCallNo = selectedRows.reduce((sum, val) => {
			return sum + parseFloat(val.callNo, 10);
		}, 0);

		if (this.props.onSelectRow) {
			this.props.onSelectRow(selectedRows);
		}

		this.setState({ selectedRowKeys, totalCallNo });
	}

	handleTableChange = (pagination, filters, sorter) => {
		this.props.onChange(pagination, filters, sorter);
	}

	cleanSelectedKeys = () => {
		this.handleRowSelectChange([], []);
	}

	render() {
		const { selectedRowKeys, totalCallNo } = this.state;
		const { data: { list, pagination }, loading } = this.props;

		const platform = [,'支付宝','微信','余额'];

		const columns = [
			{
				title: 'ID',
				dataIndex: 'key',
			},
			{
				title: '订单号',
				dataIndex: 'order_id',
			},
			{
				title: '头像',
				dataIndex: 'head',
				render(val) {
					if (val == null) {
						return <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />;
					}
					var result = val.indexOf("http");
					if (result != 0) {
						val = 'http://image2.tiandixindao.com/' + val;
					}
					return <Avatar src={val} />
				}
			},
			{
				title: '用户名ID',
				dataIndex: 'mid',
			},
			{
				title: '用户名',
				dataIndex: 'uname',
			},
			{
				title: '操作行为',
				dataIndex: 'rule_name',
			},
			{
				title: '金额',
				dataIndex: 'change_money',
				render(val){
					return val/100;
				}
			},
			{
				title: '支付方式',
				dataIndex: 'pay_platform_id',
				render(val) {
					return <Badge status={statusMap[val]} text={platform[val]} />;
				},
			},
			{
				title: '创建时间',
				dataIndex: 'date_create',
			},
		];

		const paginationProps = {
			showSizeChanger: true,
			showQuickJumper: true,
			...pagination,
		};

		const rowSelection = {
			selectedRowKeys,
			onChange: this.handleRowSelectChange,
			getCheckboxProps: record => ({
				disabled: record.disabled,
			}),
		};

		return (
			<div className={styles.standardTable}>
				{/* <div className={styles.tableAlert}>
					<Alert
						message={(
							<div>
								已选择 <a style={{ fontWeight: 600 }}>{selectedRowKeys.length}</a> 项&nbsp;&nbsp;
                服务调用总计 <span style={{ fontWeight: 600 }}>{totalCallNo}</span> 万
                <a onClick={this.cleanSelectedKeys} style={{ marginLeft: 24 }}>清空</a>
							</div>
						)}
						type="info"
						showIcon
					/>
				</div> */}
				<Table
					loading={loading}
					rowKey={record => record.key}
					// rowSelection={rowSelection}
					dataSource={list}
					columns={columns}
					pagination={paginationProps}
					onChange={this.handleTableChange}
				/>
			</div>
		);
	}
}

export default StandardTable;
