import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Table, Badge, Avatar, Button, Modal, message, Select, Input } from 'antd';
import styles from './index.less';

const { TextArea } = Input;
const Option = Select.Option;
const statusMap = ['', 'processing', 'success', 'error', 'default'];
@connect(state => ({
  withdrawal: state.withdrawal,
}))
class StandardTable extends PureComponent {
    state = {
      selectedRowKeys: [],
      totalCallNo: 0,
      ModalText: null,
      visible: false,
      confirmLoading: false,
      disableMid: null,
      statusExplan: null,
    };

    componentWillReceiveProps(nextProps) {
      // clean state
      if (nextProps.selectedRows.length === 0) {
        this.setState({
          selectedRowKeys: [],
          totalCallNo: 0,
        });
      }
    }

    handleChange = (value) => {
      this.setState({ statusExplan: value });
      console.log(`selected ${value}`);
    }

    showModal = (id, name) => {
      this.setState({
        visible: true,
        ModalText: `是否删除 ${name}?`,
        disableMid: id,
      });
    }

    handleCancel = () => {
      this.setState({
        visible: false,
        disableMid: null,
      });
    }

    handleOk = (mid) => {
      const { dispatch } = this.props;
      this.setState({
        ModalText: '正在执行...',
        confirmLoading: true,
      });
      dispatch({
        type: 'withdrawal/dealWith',
        payload: {
          mid,
          status: this.state.statusExplan,
          statusExplan: document.getElementById('fail').value,
        },
        callback: (response) => {
          if (response.backcode === 200) {
            message.success(response.backdata);
            dispatch({
              type: 'withdrawal/fetch',
              payload: {
                currentPage: this.props.data.pagination.current,
                pageSize: this.props.data.pagination.pageSize,
              },
            });
          } else {
            message.error(response.msg);
          }
          this.setState({
            visible: false,
            confirmLoading: false,
          });
        },
      });
    }


    handleTableChange = (pagination, filters, sorter) => {
      this.props.onChange(pagination, filters, sorter);
    }


    render() {
      const { selectedRowKeys, totalCallNo } = this.state;
      const { data: { list, pagination }, loading } = this.props;
      const { visible, confirmLoading, ModalText, disableMid } = this.state;


      const platform = ['', '支付宝'];
      const withdrawalStatus = ['', '提现中', '提现成功', '提现失败'];

      const columns = [
        {
          title: 'ID',
          dataIndex: 'key',
          fixed: 'left',
          width: 100,
        },
        {
          title: '头像',
          dataIndex: 'head',
          width: 150,
          render(val) {
            if (val == null) {
              return <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />;
            }
            const result = val.indexOf('http');
            if (result !== 0) {
              val = `http://image2.tiandixindao.com/${val}`;
            }
            return <Avatar src={val} />;
          },
        },
        {
          title: '用户名ID',
          dataIndex: 'mid',
          width: 150,
        },
        {
          title: '用户名',
          dataIndex: 'uname',
          width: 150,
        },
        {
          title: '状态',
          dataIndex: 'status',
          width: 150,
          render(val) {
            return <Badge status={statusMap[val]} text={withdrawalStatus[val]} />;
          },
        },
        {
          title: '提现金额',
          dataIndex: 'money',
          width: 150,
        },
        {
          title: '到账金额',
          dataIndex: 'finally_money',
          width: 150,
        },
        {
          title: '支付平台',
          dataIndex: 'platform_id',
          width: 150,
          render(val) {
            return <Badge status={statusMap[val]} text={platform[val]} />;
          },
        },
        {
          title: '支付账号',
          dataIndex: 'account',
          width: 200,
        },
        {
          title: '支付账号姓名',
          dataIndex: 'actual_name',
          width: 150,
        },
        {
          title: '进度说明',
          dataIndex: 'status_explan',
          width: 300,
        },
        {
          title: '创建时间',
          dataIndex: 'date_create',
          width: 200,
        },
        {
          title: '更新时间',
          dataIndex: 'date_operate',
          width: 200,
        },
        {
          title: '操作',
          align: 'center',
          fixed: 'right',
          width: 150,
          render: (text, record) => {
            if (record.status === 1) {
              return <Button type="primary" onClick={() => this.showModal(record.mid, record.name)}>待处理</Button>;
            } else {
              return <Button type="primary" disabled>已处理</Button>;
            }
          },
        },
      ];

      const paginationProps = {
        showSizeChanger: true,
        showQuickJumper: true,
        ...pagination,
      };

      return (
        <div className={styles.standardTable}>
          <Table
            loading={loading}
            rowKey={record => record.key}
            dataSource={list}
            scroll={{ x: 3000 }}
            columns={columns}
            pagination={paginationProps}
            onChange={this.handleTableChange}
          />
          <Modal
            title="提现"
            visible={visible}
            onOk={() => this.handleOk(disableMid)}
            confirmLoading={confirmLoading}
            onCancel={this.handleCancel}
          >
                    提现状态：
            <Select
              title=""
              showSearch
              style={{ width: 200 }}
              placeholder="修改提现状态"
              name="status"
              onChange={this.handleChange}
            >
              <Option value="2">提现成功</Option>
              <Option value="3">提现失败</Option>
            </Select>
            <br />
            <br />
            <p style={{ display: this.state.statusExplan === '3' ? 'block' : 'none' }}>失败原因：<TextArea id="fail" style={{ width: 200, height: 150 }} placeholder="请输入失败原因" /></p>
          </Modal>
        </div>
      );
    }
}

export default StandardTable;
