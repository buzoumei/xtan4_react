import React, { PureComponent } from 'react';
import moment from 'moment';
import { Table, Alert, Badge, Divider, Avatar } from 'antd';
import styles from './index.less';
import { stat } from 'fs';

const statusMap = ['success', 'processing', 'default', 'error'];
class StandardTable extends PureComponent {
	state = {
		selectedRowKeys: [],
		totalCallNo: 0,
	};

	componentWillReceiveProps(nextProps) {
		// clean state
		if (nextProps.selectedRows.length === 0) {
			this.setState({
				selectedRowKeys: [],
				totalCallNo: 0,
			});
		}
	}

	handleRowSelectChange = (selectedRowKeys, selectedRows) => {
		const totalCallNo = selectedRows.reduce((sum, val) => {
			return sum + parseFloat(val.callNo, 10);
		}, 0);

		if (this.props.onSelectRow) {
			this.props.onSelectRow(selectedRows);
		}

		this.setState({ selectedRowKeys, totalCallNo });
	}

	handleTableChange = (pagination, filters, sorter) => {
		this.props.onChange(pagination, filters, sorter);
	}

	cleanSelectedKeys = () => {
		this.handleRowSelectChange([], []);
	}

	render() {
		const { selectedRowKeys, totalCallNo } = this.state;
		const { data: { list, pagination }, loading } = this.props;

		// const status = ['关闭', '运行中', '已上线', '异常'];
		const status = ['正常','封禁'];

		const columns = [
			{
				title: 'ID',
				'width':50,
				dataIndex: 'key',
			},
			{
				title: 'question',
				dataIndex: 'question',
				render: (val) => {
					return val.substring(0, 60);
				},
			},
			{
				title: 'answer',
				dataIndex: 'answer',
				render:(val)=>{
					return val.substring(0, 60);
				},
			},
			{
				title: '操作',
				// align: 'center',
				render: () => (
					<div>
						<a href="">编辑</a>
						<Divider type="vertical" />
						<a href="">删除	</a>
					</div>
				),
			},
		];

		const paginationProps = {
			showSizeChanger: true,
			showQuickJumper: true,
			...pagination,
		};

		const rowSelection = {
			selectedRowKeys,
			onChange: this.handleRowSelectChange,
			getCheckboxProps: record => ({
				disabled: record.disabled,
			}),
		};

		return (
			<div className={styles.standardTable}>
				<Table
					loading={loading}
					rowKey={record => record.key}
					dataSource={list}
					columns={columns}
					pagination={paginationProps}
					onChange={this.handleTableChange}
				/>
			</div>
		);
	}
}

export default StandardTable;
