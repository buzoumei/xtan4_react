import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Table, Badge, Avatar, Modal, Button, message, Select } from 'antd';
import styles from './index.less';

const Option = Select.Option;
const statusMap = ['default', 'processing', 'success', 'error'];
@connect(state => ({
  member: state.member,
}))
class StandardTable extends PureComponent {
	state = {
	  selectedRowKeys: [],
	  totalCallNo: 0,
	  ModalText: null,
	  visible: false,
	  confirmLoading: false,
	  disableMid: null,
	  disabledTime: null,
	};

	handleChange = (value) => {
	  this.setState({ disabledTime: value });
	  console.log(`selected ${value}`);
	}

	componentWillReceiveProps(nextProps) {
	  // clean state
	  if (nextProps.selectedRows.length === 0) {
	    this.setState({
	      selectedRowKeys: [],
	      totalCallNo: 0,
	    });
	  }
	}

	showModal = (id, name) => {
	  this.setState({
	    visible: true,
	    ModalText: `是否删除 ${name}?`,
	    disableMid: id,
	  });
	}

	handleCancel = () => {
	  console.log('您取消了删除');
	  this.setState({
	    visible: false,
	    disableMid: null,
	  });
	}

	handleOk = (disableMid) => {
	  const { dispatch } = this.props;
	  this.setState({
	    ModalText: '正在执行...',
	    confirmLoading: true,
	  });
	  dispatch({
	    type: 'member/disable',
	    payload: {
	      disableMid,
	      disabledTime: this.state.disabledTime,
	    },
	    callback: (response) => {
	      if (response.status) {
	        message.success(response.msg);
	        dispatch({
	          type: 'member/fetch',
	          payload: {
	            currentPage: this.props.data.pagination.current,
	            pageSize: this.props.data.pagination.pageSize,
	          },
	        });
	      } else {
	        message.error(response.msg);
	      }
	      this.setState({
	        visible: false,
	        confirmLoading: false,
	      });
	    },
	  });
	}

	unDisable = (disableMid) => {
	  const { dispatch } = this.props;
	  this.setState({
	    ModalText: '正在执行...',
	    confirmLoading: true,
	  });
	  dispatch({
	    type: 'member/undisable',
	    payload: {
	      disableMid,
	    },
	    callback: (response) => {
	      if (response.status) {
	        message.success(response.msg);
	        dispatch({
	          type: 'member/fetch',
	          payload: {
	            currentPage: this.props.data.pagination.current,
	            pageSize: this.props.data.pagination.pageSize,
	          },
	        });
	      } else {
	        message.error(response.msg);
	      }
	      this.setState({
	        visible: false,
	        confirmLoading: false,
	      });
	    },
	  });
	}

	handleRowSelectChange = (selectedRowKeys, selectedRows) => {
	  const totalCallNo = selectedRows.reduce((sum, val) => {
	    return sum + parseFloat(val.callNo, 10);
	  }, 0);

	  if (this.props.onSelectRow) {
	    this.props.onSelectRow(selectedRows);
	  }

	  this.setState({ selectedRowKeys, totalCallNo });
	}

	handleTableChange = (pagination, filters, sorter) => {
	  this.props.onChange(pagination, filters, sorter);
	}

	cleanSelectedKeys = () => {
	  this.handleRowSelectChange([], []);
	}

	render() {
	  const { selectedRowKeys, totalCallNo } = this.state;
	  const { data: { list, pagination }, loading } = this.props;
	  const { visible, confirmLoading, ModalText, disableMid } = this.state;

	  const status = [];
	  status[1] = '正常';
	  status[2] = '封禁';

	  const columns = [
	    {
	      title: 'ID',
	      dataIndex: 'key',
	      fixed: 'left',
	    },
	    {
	      title: '头像',
	      dataIndex: 'head',
	      render(val) {
	        if (val == null) {
	          return <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />;
	        }
	        const result = val.indexOf('http');
	        if (result != 0) {
	          val = `http://image2.tiandixindao.com/${val}`;
	        }
	        return <Avatar src={val} />;
	      },
	    },
	    {
	      title: '用户名',
	      dataIndex: 'uname',
	      width: 150,
	    },
	    {
	      title: '手机',
	      dataIndex: 'phone',
	      width: 200,
	    },
	    {
	      title: '性别',
	      dataIndex: 'sex',
	      align: 'center',
	      width: 150,
	      render(val) {
	        if (val == 1) {
	          return '男';
	        }
	        if (val == 2) {
	          return '女';
	        }
	        return '保密';
	      },
	    },
	    {
	      title: '文章数量',
	      align: 'center',
	      dataIndex: 'dynamicCount',
	    },
	    {
	      title: '关注数量',
	      align: 'center',
	      dataIndex: 'attentionCount',
	    },
	    {
	      title: '粉丝数量',
	      align: 'center',
	      dataIndex: 'fansCount',
	    },
	    {
	      title: '等级',
	      align: 'center',
	      dataIndex: 'level',
	    },
	    {
	      title: '余额',
	      align: 'center',
	      dataIndex: 'money',
	      render: val => <span>{val / 100}</span>,
	    },
	    {
	      title: '积分',
	      align: 'center',
	      dataIndex: 'score',
	    },
	    {
	      title: '角色',
	      dataIndex: 'roleId',
	      width: 150,
	      render(val) {
	        if (val == 0) {
	          return '普通用户';
	        }
	        if (val == 1 || val == 2) {
	          return '管理员';
	        }
	        if (val == 3) {
	          return '超级管理员';
	        }
	      },
	    },
	    {
	      title: '生日',
	      dataIndex: 'birthday',
	      sorter: true,
	    },
	    {
	      title: '状态',
	      dataIndex: 'status',
	      width: 200,
	      filters: [
	        {
	          text: status[1],
	          value: 1,
	        },
	        {
	          text: status[2],
	          value: 2,
	        },
	      ],
	      render(val) {
	        return <Badge status={statusMap[val]} text={status[val]} />;
	      },
	    },
	    {
	      title: '操作',
	      align: 'center',
	      fixed: 'right',
	      width: 100,
	      render: (text, record) => {
	        if (record.status == 2) {
	          return <Button type="primary" onClick={() => this.unDisable(record.key, record.name)}>解封</Button>;
	        } else {
	          return <Button type="danger" onClick={() => this.showModal(record.key, record.name)}>封禁</Button>;
	        }
	      },
	    },
	  ];

	  const paginationProps = {
	    showSizeChanger: true,
	    showQuickJumper: true,
	    ...pagination,
	  };

	  const rowSelection = {
	    selectedRowKeys,
	    onChange: this.handleRowSelectChange,
	    getCheckboxProps: record => ({
	      disabled: record.disabled,
	    }),
	  };

	  return (
  <div className={styles.standardTable}>
    <Table
      loading={loading}
      rowKey={record => record.key}
      dataSource={list}
      columns={columns}
      pagination={paginationProps}
      onChange={this.handleTableChange}
      scroll={{ x: 3000 }}
    />
    <Modal
      title="封禁"
      visible={visible}
      onOk={() => this.handleOk(disableMid)}
      confirmLoading={confirmLoading}
      onCancel={this.handleCancel}
    >
      <Select
        showSearch
        style={{ width: 200 }}
        placeholder="选择封禁时长"
        optionFilterProp="children"
        name="diabledTime"
        onChange={this.handleChange}
        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
      >
        <Option value="+ 1 day">一天</Option>
        <Option value="+ 3 day">三天</Option>
        <Option value="+ 10 days">十天</Option>
        <Option value="+ 1 month">一个月</Option>
        <Option value="+ 1 year">一年</Option>
        <Option value="+ 10 year">十年</Option>
      </Select>
    </Modal>
  </div>
	  );
	}
}

export default StandardTable;
