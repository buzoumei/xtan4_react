import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Row, Col, Card, Form, Input, Select, Icon, Button, Dropdown, Menu, InputNumber, DatePicker, Modal, message } from 'antd';
import StandardTable from './components/StandardTable';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

import styles from './index.less';

const FormItem = Form.Item;
const { Option } = Select;
const { RangePicker } = DatePicker;
const getValue = obj => Object.keys(obj).map(key => obj[key]).join(',');

@connect(state => ({
	member: state.member,
}))
@Form.create()
export default class TableList extends PureComponent {
	state = {
		addInputValue: '',
		modalVisible: false,
		expandForm: false,
		selectedRows: [],
		formValues: {},
	};

	componentDidMount() {
		const { dispatch } = this.props;
		dispatch({
			type: 'member/fetch',
		});
	}

	handleStandardTableChange = (pagination, filtersArg, sorter) => {
		const { dispatch } = this.props;
		const { formValues } = this.state;

		const filters = Object.keys(filtersArg).reduce((obj, key) => {
			const newObj = { ...obj };
			newObj[key] = getValue(filtersArg[key]);
			return newObj;
		}, {});

		const params = {
			currentPage: pagination.current,
			pageSize: pagination.pageSize,
			...formValues,
			...filters,
		};
		if (sorter.field) {
			params.sorter = `${sorter.field}_${sorter.order}`;
		}

		dispatch({
			type: 'member/fetch',
			payload: params,
		});
	}

	handleFormReset = () => {
		const { form, dispatch } = this.props;
		form.resetFields();
		this.setState({
			formValues: {},
		});
		dispatch({
			type: 'member/fetch',
			payload: {},
		});
	}

	toggleForm = () => {
		this.setState({
			expandForm: !this.state.expandForm,
		});
	}

	handleMenuClick = (e) => {
		const { dispatch } = this.props;
		const { selectedRows } = this.state;

		if (!selectedRows) return;

		switch (e.key) {
			case 'remove':
				dispatch({
					type: 'member/remove',
					payload: {
						no: selectedRows.map(row => row.no).join(','),
					},
					callback: () => {
						this.setState({
							selectedRows: [],
						});
					},
				});
				break;
			default:
				break;
		}
	}

	handleSelectRows = (rows) => {
		this.setState({
			selectedRows: rows,
		});
	}

	handleSearch = (e) => {
		e.preventDefault();

		const { dispatch, form } = this.props;

		form.validateFields((err, fieldsValue) => {
			if (err) return;

			const values = {
				...fieldsValue,
				updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
			};

			this.setState({
				formValues: values,
			});

			dispatch({
				type: 'member/fetch',
				payload: values,
			});
		});
	}

	handleModalVisible = (flag) => {
		this.setState({
			modalVisible: !!flag,
		});
	}

	handleAddInput = (e) => {
		this.setState({
			addInputValue: e.target.value,
		});
	}

	handleAdd = () => {
		this.props.dispatch({
			type: 'member/add',
			payload: {
				description: this.state.addInputValue,
			},
		});

		message.success('添加成功');
		this.setState({
			modalVisible: false,
		});
	}

	renderSimpleForm() {
		const { getFieldDecorator } = this.props.form;
		return (
			<Form onSubmit={this.handleSearch} layout="inline">
				<Row gutter={{ md: 8, lg: 24, xl: 48 }}>
					<Col md={8} sm={24}>
						<FormItem label="用户昵称">
							{getFieldDecorator('uname')(
								<Input placeholder="请输入用户昵称" />
							)}
						</FormItem>
					</Col>
					<Col md={8} sm={24}>
						<FormItem label="用户手机">
							{getFieldDecorator('phone')(
								<Input placeholder="请输入phone" />
							)}
						</FormItem>
					</Col>
					<Col md={8} sm={24}>
						<span className={styles.submitButtons}>
							<Button type="primary" htmlType="submit">查询</Button>
							<Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
							<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
								展开 <Icon type="down" />
							</a>
						</span>
					</Col>
				</Row>
			</Form>
		);
	}

	renderAdvancedForm() {
		const { getFieldDecorator } = this.props.form;
		return (
			<Form onSubmit={this.handleSearch} layout="inline">
				<Row gutter={{ md: 8, lg: 24, xl: 48 }}>
					<Col md={8} sm={24}>
						<FormItem label="用户昵称">
							{getFieldDecorator('uname')(
								<Input placeholder="请输入用户昵称" />
							)}
						</FormItem>
					</Col>
					<Col md={8} sm={24}>
						<FormItem label="用户手机">
							{getFieldDecorator('phone')(
								<Input placeholder="请输入phone" />
							)}
						</FormItem>
					</Col>
					<Col md={8} sm={24}>
						<FormItem label="用户性别">
							{getFieldDecorator('sex')(
								<Select placeholder="请选择" style={{ width: '100%' }}>
									<Option value="1">男</Option>
									<Option value="2">女</Option>
									<Option value="0">未知</Option>
								</Select>
							)}
						</FormItem>
					</Col>
				</Row>
				<Row gutter={{ md: 8, lg: 24, xl: 48 }}>
					<Col md={8} sm={24}>
						<FormItem label="登录时间">
							{getFieldDecorator('date')(
								<RangePicker style={{ width: '100%' }}/>
							)}
						</FormItem>
					</Col>
					<Col md={8} sm={24}>
						<FormItem label="用户状态">
							{getFieldDecorator('status3')(
								<Select placeholder="请选择" style={{ width: '100%' }}>
									<Option value="2">封禁</Option>
									<Option value="1">正常</Option>
								</Select>
							)}
						</FormItem>
					</Col>
					<Col md={8} sm={24}>
						<FormItem label="用户权限">
							{getFieldDecorator('roleId')(
								<Select placeholder="请选择" style={{ width: '100%' }}>
									<Option value="0">普通用户</Option>
									<Option value="1">管理员1</Option>
									<Option value="2">管理员2</Option>
									<Option value="3">超级管理员</Option>
								</Select>
							)}
						</FormItem>
					</Col>
				</Row>
				<div style={{ overflow: 'hidden' }}>
					<span style={{ float: 'right', marginBottom: 24 }}>
						<Button type="primary" htmlType="submit">查询</Button>
						<Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>重置</Button>
						<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
							收起 <Icon type="up" />
						</a>
					</span>
				</div>
			</Form>
		);
	}

	renderForm() {
		return this.state.expandForm ? this.renderAdvancedForm() : this.renderSimpleForm();
	}

	render() {
		const { member: { loading: ruleLoading, data } } = this.props;
		const { selectedRows, modalVisible, addInputValue } = this.state;

		const menu = (
			<Menu onClick={this.handleMenuClick} selectedKeys={[]}>
				<Menu.Item key="remove">删除</Menu.Item>
				<Menu.Item key="approval">批量审批</Menu.Item>
			</Menu>
		);

		return (
			<PageHeaderLayout>
				<Card bordered={false}>
					<div className={styles.tableList}>
						<div className={styles.tableListForm}>
							{this.renderForm()}
						</div>
						<div className={styles.tableListOperator}>
							{/* <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
								新建
             				</Button> */}
							{
								selectedRows.length > 0 && (
									<span>
										<Button>批量操作</Button>
										<Dropdown overlay={menu}>
											<Button>
												更多操作 <Icon type="down" />
											</Button>
										</Dropdown>
									</span>
								)
							}
						</div>
						<StandardTable
							selectedRows={selectedRows}
							loading={ruleLoading}
							data={data}
							onSelectRow={this.handleSelectRows}
							onChange={this.handleStandardTableChange}
						/>
					</div>
				</Card>
				<Modal
					title="新建规则"
					visible={modalVisible}
					onOk={this.handleAdd}
					onCancel={() => this.handleModalVisible()}
				>
					<FormItem
						labelCol={{ span: 5 }}
						wrapperCol={{ span: 15 }}
						label="描述"
					>
						<Input placeholder="请输入" onChange={this.handleAddInput} value={addInputValue} />
					</FormItem>
				</Modal>
			</PageHeaderLayout>
		);
	}
}
