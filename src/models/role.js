import { message } from 'antd';
import { routerRedux } from 'dva/router';
import { queryRoles, queryRoleMenus, addRole, editRole, queryRoleDetail, removeRole } from '../services/api';


export default {
  namespace: 'role',
  state: {
    data: {
      list: [],
      pagination: {},
    },
    roles: {},
    loading: true,
    regularFormSubmitting: false,
    roleMenus: [],
    roleMenusLoading: true,
    detail: [],
    detailLoading: true,
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(queryRoles, payload);
      yield put({
        type: 'queryRoles',
        payload: response,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
    *roleMenus({ payload }, { call, put }) {
      yield put({
        type: 'changeRoleMenusLoading',
        payload: true,
      });
      const response = yield call(queryRoleMenus, payload);
      yield put({
        type: 'roleMenusSave',
        payload: response,
      });
      yield put({
        type: 'changeRoleMenusLoading',
        payload: false,
      });
    },
    *roleDetail({ payload }, { call, put }) {
      yield put({
        type: 'changeDetailLoading',
        payload: true,
      });
      const response = yield call(queryRoleDetail, payload);
      yield put({
        type: 'roleDetailSave',
        payload: response,
      });
      if (!response.status) {
        message.error(response.msg);
        yield put(routerRedux.push('/admin/role'));
      } else {
        yield put({
          type: 'changeDetailLoading',
          payload: false,
        });
      }
    },
    *add({ payload }, { call, put }) {
      yield put({
        type: 'changeRegularFormSubmitting',
        payload: true,
      });
      const response = yield call(addRole, payload);
      if (response.status) {
        message.success('提交成功');
        yield put(routerRedux.push('/admin/role'));
      } else {
        message.error(response.msg);
        yield put({
          type: 'changeRegularFormSubmitting',
          payload: false,
        });
      }
    },
    *edit({ payload }, { call, put }) {
      yield put({
        type: 'changeRegularFormSubmitting',
        payload: true,
      });
      const response = yield call(editRole, payload);
      if (response.status == true) {
        message.success(response.msg);
        yield put(routerRedux.push('/admin/role'));
      } else {
        message.error(response.msg);
        yield put({
          type: 'changeRegularFormSubmitting',
          payload: false,
        });
      }
    },
    *remove({ payload, callback }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(removeRole, payload);
      if (callback) callback(response);
    },
  },

  reducers: {
    queryRoles(state, action) {
      return {
        ...state,
        data: action.payload.list,
      };
    },
    rolesSave(state, action) {
      return {
        ...state,
        roles: action.payload.list,
      };
    },
    roleDetailSave(state, action) {
      return {
        ...state,
        detail: action.payload.list,
      };
    },
    changeLoading(state, action) {
      return {
        ...state,
        loading: action.payload,
      };
    },
    changeRegularFormSubmitting(state, { payload }) {
      return {
        ...state,
        regularFormSubmitting: payload,
      };
    },
    // role权限菜单
    roleMenusSave(state, action) {
      return {
        ...state,
        roleMenus: action.payload,
      };
    },
    changeRoleMenusLoading(state, { payload }) {
      return {
        ...state,
        roleMenusLoading: payload,
      };
    },
    changeDetailLoading(state, action) {
      return {
        ...state,
        detailLoading: action.payload,
      };
    },
  },
};
