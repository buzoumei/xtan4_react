import { queryRules, queryRoles, removeUser, addUser } from '../services/api';
import { message } from 'antd';
import { routerRedux } from 'dva/router';


export default {
  namespace: 'question',
  state: {
    data: {
      list: [],
      pagination: {},
    },
    roles: {},
    loading: true,
    regularFormSubmitting: false,
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(queryRules, payload);
      yield put({
        type: 'saveRules',
        payload: response,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
  },

  reducers: {
    saveRules(state, action) {
      return {
        ...state,
        data: action.payload.list,
      };
    },
    rolesSave(state, action) {
      return {
        ...state,
        roles: action.payload.list,
      };
    },
    changeLoading(state, action) {
      return {
        ...state,
        loading: action.payload,
      };
    },
    changeRegularFormSubmitting(state, { payload }) {
      return {
        ...state,
        regularFormSubmitting: payload,
      };
    },
  },
};
