import { queryWithdrawals, dealWithdrawal, exportWithdrawal } from '../services/api';


export default {
  namespace: 'withdrawal',
  state: {
    data: {
      list: [],
      pagination: {},
    },
    roles: {},
    loading: true,
    regularFormSubmitting: false,
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(queryWithdrawals, payload);
      yield put({
        type: 'saveWithdrawals',
        payload: response,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
    *dealWith({ payload, callback }, { call, put }) {
      const response = yield call(dealWithdrawal, payload);
      if (callback) callback(response);
    },
    *export({ payload, callback }, { call, put }) {
      const response = yield call(exportWithdrawal, payload);
      if (callback) callback(response);
    },
  },

  reducers: {
    saveWithdrawals(state, action) {
      return {
        ...state,
        data: action.payload.list,
      };
    },
    changeLoading(state, action) {
      return {
        ...state,
        loading: action.payload,
      };
    },
  },
};
