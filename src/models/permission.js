import { message } from 'antd';
import { routerRedux } from 'dva/router';
import { queryRoleMenus, queryPermissions, queryPermissionDetail, removePermission, addPermission, editPermission } from '../services/api';


export default {
  namespace: 'permission',
  state: {
    data: {
      list: [],
      pagination: {},
    },
    roles: {},
    loading: true,
    regularFormSubmitting: false,
    menus: [],
    menusLoading: false,
    permissionDetail: [],
    detailLoading: true,
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(queryPermissions, payload);
      yield put({
        type: 'saveQueryPermissions',
        payload: response,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
    *menus({ payload }, { call, put }) {
      yield put({
        type: 'changeMenusLoading',
        payload: true,
      });
      const response = yield call(queryRoleMenus, payload);
      yield put({
        type: 'menusSave',
        payload: response,
      });
      yield put({
        type: 'changeMenusLoading',
        payload: false,
      });
    },
    *permissionDetail({ payload }, { call, put }) {
      yield put({
        type: 'changeDetailLoading',
        payload: true,
      });
      // console.log(payload);
      const response = yield call(queryPermissionDetail, payload);
      yield put({
        type: 'permissionDetailSave',
        payload: response,
      });
      if (!response.status) {
        message.error(response.msg);
        yield put(routerRedux.push('/admin/permission'));
      } else {
        yield put({
          type: 'changeDetailLoading',
          payload: false,
        });
      }
    },
    *add({ payload }, { call, put }) {
      yield put({
        type: 'changeRegularFormSubmitting',
        payload: true,
      });
      const response = yield call(addPermission, payload);
      if (response.status == true) {
        message.success(response.msg);
        yield put(routerRedux.push('/admin/permission'));
      } else {
        message.error(response.msg);
      }
      yield put({
        type: 'changeRegularFormSubmitting',
        payload: false,
      });
    },
    *edit({ payload }, { call, put }) {
      yield put({
        type: 'changeRegularFormSubmitting',
        payload: true,
      });
      const response = yield call(editPermission, payload);
      if (response.status == true) {
        message.success(response.msg);
        yield put(routerRedux.push('/admin/permission'));
      } else {
        message.error(response.msg);
      }
      yield put({
        type: 'changeRegularFormSubmitting',
        payload: false,
      });
    },
    *remove({ payload, callback }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(removePermission, payload);
      if (callback) callback(response);
    },
  },

  reducers: {
    saveQueryPermissions(state, action) {
      return {
        ...state,
        data: action.payload.list,
      };
    },
    menusSave(state, action) {
      return {
        ...state,
        menus: action.payload,
      };
    },
    permissionDetailSave(state, action) {
      return {
        ...state,
        permissionDetail: action.payload,
      };
    },
    changeMenusLoading(state, action) {
      return {
        ...state,
        menusLoading: action.payload,
      };
    },
    changeLoading(state, action) {
      return {
        ...state,
        loading: action.payload,
      };
    },
    changeDetailLoading(state, action) {
      return {
        ...state,
        detailLoading: action.payload,
      };
    },
    changeRegularFormSubmitting(state, { payload }) {
      return {
        ...state,
        regularFormSubmitting: payload,
      };
    },
  },
};
