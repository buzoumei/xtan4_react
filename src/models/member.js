import { queryMember, removeRule, disableMember, undisableMember } from '../services/api';

export default {
  namespace: 'member',

  state: {
    data: {
      list: [],
      pagination: {},
    },
    loading: true,
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(queryMember, payload);
      yield put({
        type: 'saveMembers',
        payload: response,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
    *disable({ payload, callback }, { call, put }) {
      const response = yield call(disableMember, payload);
      if (callback) callback(response);
    },
    *undisable({ payload, callback }, { call, put }) {
      const response = yield call(undisableMember, payload);
      if (callback) callback(response);
    },
    *remove({ payload, callback }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(removeRule, payload);
      yield put({
        type: 'save',
        payload: response,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });

      if (callback) callback();
    },
  },

  reducers: {
    saveMembers(state, action) {
      return {
        ...state,
        data: action.payload.list,
      };
    },
    changeLoading(state, action) {
      return {
        ...state,
        loading: action.payload,
      };
    },
  },
};
