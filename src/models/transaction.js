import { message } from 'antd';
import { routerRedux } from 'dva/router';
import { queryTransactions, queryRoles, removeUser, addUser } from '../services/api';


export default {
  namespace: 'transaction',
  state: {
    data: {
      list: [],
      pagination: {},
    },
    roles: {},
    loading: true,
    regularFormSubmitting: false,
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(queryTransactions, payload);
      yield put({
        type: 'saveTransactions',
        payload: response,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
    *roles({ payload }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(queryRoles, payload);
      yield put({
        type: 'rolesSave',
        payload: response,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
  },

  reducers: {
    saveTransactions(state, action) {
      return {
        ...state,
        data: action.payload.list,
      };
    },
    rolesSave(state, action) {
      return {
        ...state,
        roles: action.payload.list,
      };
    },
    changeLoading(state, action) {
      return {
        ...state,
        loading: action.payload,
      };
    },
    changeRegularFormSubmitting(state, { payload }) {
      return {
        ...state,
        regularFormSubmitting: payload,
      };
    },
  },
};
