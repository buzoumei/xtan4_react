// import { routerRedux } from 'dva/router';
import { message } from 'antd';
import { fakeAccountLogin, queryCurrentMenus } from '../services/api';
import { setAuthority } from '../utils/authority';


export default {
  namespace: 'login',

  state: {
    status: undefined,
  },

  effects: {
    *login({ payload }, { call, put }) {
      const response = yield call(fakeAccountLogin, payload);
      yield put({
        type: 'changeLoginStatus',
        payload: response,
      });
      console.log(response);
      if (response.status) {
        message.success(response.msg);
        const currentMenus = yield call(queryCurrentMenus);
        console.log(currentMenus);

        if (!currentMenus.status) {
          console.log('获取用户菜单错误');
        }
        sessionStorage.setItem('menuData', JSON.stringify(currentMenus.list));
        if (sessionStorage.getItem('menuData')) {
          //   console.log('run');
          window.location.reload();
        //   yield put(routerRedux.push('/'));
        } else {
          console.log('存储用户权限菜单错误');
        }
      } else {
        message.error(response.msg);
      }
      // Login successfully
      //   if (response.status) {
      //     // 非常粗暴的跳转,登陆成功之后权限会变成user或admin,会自动重定向到主页
      //     // Login success after permission changes to admin or user
      //     // The refresh will automatically redirect to the home page
      //     // yield put(routerRedux.push('/'));
      //     window.location.reload();
      //   }
    },
    *logout(_, { put }) {
      sessionStorage.removeItem('menuData');
      yield put({
        type: 'changeLoginStatus',
        payload: {
          status: false,
          list: {
            currentAuthority: 'guest',
          },
        },
      });
      // yield put(routerRedux.push('/user/login'));
      // Login out after permission changes to admin or user
      // The refresh will automatically redirect to the login page
      window.location.reload();
    },
  },

  reducers: {
    changeLoginStatus(state, { payload }) {
      setAuthority(payload.list.currentAuthority);
      return {
        ...state,
        status: payload.status,
        type: payload.type,
      };
    },
  },
};
