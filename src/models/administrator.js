import { message } from 'antd';
import { routerRedux } from 'dva/router';
import { queryAdministrators, queryAdministratorDetail, querySimpleRoles, removeAdministrator, addAdministrator, editAdministrator } from '../services/api';


export default {
  namespace: 'administrator',
  state: {
    data: {
      list: [],
      pagination: {},
    },
    roles: [],
    rolesLoading: true,
    loading: true,
    regularFormSubmitting: false,
    detailLoading: true,
    detail: [],
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(queryAdministrators, payload);
      if (!response.status) {
        return;
      }
      yield put({
        type: 'saveQueryAdministrators',
        payload: response,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
    *roles({ payload }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(querySimpleRoles, payload);
      yield put({
        type: 'roleSave',
        payload: response,
      });
      yield put({
        type: 'changeRolesLoading',
        payload: false,
      });
    },
    *administratorDetail({ payload }, { call, put }) {
      yield put({
        type: 'changeDetailLoading',
        payload: true,
      });
      const response = yield call(queryAdministratorDetail, payload);
      yield put({
        type: 'administratorDetailSave',
        payload: response,
      });
      if (!response.status) {
        message.error(response.msg);
        yield put(routerRedux.push('/admin/administrator'));
      } else {
        yield put({
          type: 'changeDetailLoading',
          payload: false,
        });
      }
    },
    *add({ payload }, { call, put }) {
      yield put({
        type: 'changeRegularFormSubmitting',
        payload: true,
      });
      const response = yield call(addAdministrator, payload);
      if (response.status == true) {
        message.success('提交成功');
        yield put(routerRedux.push('/admin/administrator'));
      } else {
        message.error(response.msg);
      }
      yield put({
        type: 'changeRegularFormSubmitting',
        payload: false,
      });
    },
    *edit({ payload }, { call, put }) {
      yield put({
        type: 'changeRegularFormSubmitting',
        payload: true,
      });
      const response = yield call(editAdministrator, payload);
      if (response.status == true) {
        message.success(response.msg);
        yield put(routerRedux.push('/admin/administrator'));
      } else {
        message.error(response.msg);
      }
      yield put({
        type: 'changeRegularFormSubmitting',
        payload: false,
      });
    },
    *remove({ payload, callback }, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(removeAdministrator, payload);
      if (callback) callback(response);
    },
  },

  reducers: {
    saveQueryAdministrators(state, action) {
      return {
        ...state,
        data: action.payload.list,
      };
    },
    roleSave(state, action) {
      return {
        ...state,
        roles: action.payload.list,
      };
    },
    administratorDetailSave(state, action) {
      return {
        ...state,
        detail: action.payload.list,
      };
    },
    changeLoading(state, action) {
      return {
        ...state,
        loading: action.payload,
      };
    },
    changeRolesLoading(state, action) {
      // console.log(action);
      return {
        ...state,
        rolesLoading: action.payload,
      };
    },
    changeRegularFormSubmitting(state, { payload }) {
      return {
        ...state,
        regularFormSubmitting: payload,
      };
    },
    changeDetailLoading(state, action) {
      return {
        ...state,
        detailLoading: action.payload,
      };
    },
  },
};
