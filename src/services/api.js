import { stringify } from 'qs';
import request from '../utils/request';

export async function queryProjectNotice() {
  return request('http://118.190.45.84/xtan4_admin/public/api/project/notice');
}
export async function queryFakeList() {
  // return request('http://118.190.45.84/xtan4_admin/public/api/project/notice');
}

export async function queryActivities() {
  return request('http://118.190.45.84/xtan4_admin/public/api/activities');
}

export async function queryRule(params) {
  return request(`/members?${stringify(params)}`);
}

export async function removeRule(params) {
  return request('http://118.190.45.84/xtan4_admin/public/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'delete',
    },
  });
}

export async function addRule(params) {
  return request('http://118.190.45.84/xtan4_admin/public/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}

export async function fakeSubmitForm(params) {
  return request('http://118.190.45.84/xtan4_admin/public/api/forms', {
    method: 'POST',
    body: params,
  });
}

export async function fakeChartData() {
  // return request('http://118.190.45.84/xtan4_admin/public/api/fake_chart_data');
}

export async function queryTags() {
  return request('http://118.190.45.84/xtan4_admin/public/api/tags');
}

export async function queryBasicProfile() {
  return request('http://118.190.45.84/xtan4_admin/public/api/profile/basic');
}

export async function queryAdvancedProfile() {
  return request('http://118.190.45.84/xtan4_admin/public/api/profile/advanced');
}

export async function fakeAccountLogin(params) {
  return request('http://118.190.45.84/xtan4_admin/public/api/login', {
    method: 'POST',
    body: params,
  });
}

export async function fakeRegister(params) {
  return request('http://118.190.45.84/xtan4_admin/public/api/register', {
    method: 'POST',
    body: params,
  });
}

export async function queryNotices() {
  return request('http://118.190.45.84/xtan4_admin/public/api/notices');
}


export async function queryRuleAndQuestion(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/rules?${stringify(params)}`);
}

/**
 *
 * 管理员 strat
 */
export async function queryAdministrators(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/administrators?${stringify(params)}`);
}
export async function queryAdministratorDetail(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/administrators/${params.administratorId}`);
}
export async function addAdministrator(params) {
  return request('http://118.190.45.84/xtan4_admin/public/api/administrators', {
    method: 'POST',
    body: params,
  });
}
export async function editAdministrator(params) {
  return request('http://118.190.45.84/xtan4_admin/public/api/administrators', {
    method: 'PUT',
    body: params,
  });
}
export async function removeAdministrator(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/administrators/${params.administratorId}`, {
    method: 'delete',
  });
}
export async function querySimpleRoles() {
  return request('http://118.190.45.84/xtan4_admin/public/api/roles/simple');
}
/**
 *
 * 管理员 end
 */
export async function queryPermissions(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/permissions?${stringify(params)}`);
}
export async function queryPermissionDetail(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/permissions/${params.permissionId}`);
}
export async function addPermission(params) {
  return request('http://118.190.45.84/xtan4_admin/public/api/permissions', {
    method: 'POST',
    body: params,
  });
}
export async function editPermission(params) {
  return request('http://118.190.45.84/xtan4_admin/public/api/permissions', {
    method: 'PUT',
    body: params,
  });
}
export async function removePermission(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/permissions/${params.permissionId}`, {
    method: 'delete',
  });
}
/**
 *
 * 角色 strat
 */
export async function queryRoleDetail(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/roles/${params.roleId}`);
}
export async function queryRoleMenus() {
  return request('http://118.190.45.84/xtan4_admin/public/api/roleMenus');
}
export async function queryRoles() {
  return request('http://118.190.45.84/xtan4_admin/public/api/roles');
}
export async function addRole(params) {
  return request('http://118.190.45.84/xtan4_admin/public/api/roles', {
    method: 'POST',
    body: params,
  });
}
export async function editRole(params) {
  return request('http://118.190.45.84/xtan4_admin/public/api/roles', {
    method: 'PUT',
    body: params,
  });
}
export async function removeRole(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/roles/${params.roleId}`, {
    method: 'delete',
  });
}

/**
 *
 * 角色 end
 */

export async function queryRules(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/rules?${stringify(params)}`);
}
export async function queryWithdrawals(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/withdrawals?${stringify(params)}`);
}
export async function dealWithdrawal(params) {
  return request('http://118.190.45.84/xtan4/publichttp://118.190.45.84/xtan4_admin/public/api/withdrawals', {
    method: 'PUT',
    body: params,
  });
}
export async function exportWithdrawal(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/withdrawals/export?${stringify(params)}`);
}
export async function queryTransactions(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/transactions?${stringify(params)}`);
}

export async function queryCurrentMenus() {
  return request('http://118.190.45.84/xtan4_admin/public/api/menus');
}
/**
 *
 * @param {*用户} params
 */
export async function queryMember(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/members?${stringify(params)}`);
}
export async function disableMember(params) {
  return request('http://118.190.45.84/xtan4_admin/public/api/members/disable', {
    method: 'PUT',
    body: params,
  });
}
export async function undisableMember(params) {
  return request('http://118.190.45.84/xtan4_admin/public/api/members/undisable', {
    method: 'PUT',
    body: params,
  });
}
/**
 *
 * @param {*用户} params
 */

export async function queryArticle(params) {
  return request(`http://118.190.45.84/xtan4_admin/public/api/articles?${stringify(params)}`);
}
// export async function queryArticle(params) {
// 	return request(`/articles?${stringify(params)}`);
// }
