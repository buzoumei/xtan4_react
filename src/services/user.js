import request from '../utils/request';

export async function query() {
  return request('http://118.190.45.84/xtan4_admin/public/api/users');
}

export async function queryCurrent() {
  return request('http://118.190.45.84/xtan4_admin/public/api/currentAdministrator');
}
